#!/usr/bin/enc python3
""" Homework from 11/05/2020 and 12/05/2020 """

# Import from other files here
from functions import *  # Get all functions from functions.py file
from classes import *  # Get all classes from classes.py file

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Homework from page 32
homework_msg(32)  # Print information on terminal
print(Computer(), Component(), CPU(), GPU(), PSU())  # Print information about objects

# Homework from page 41
homework_msg(41)  # Print information on terminal
author_name()  # Print author name
add_3_numbers(5, 6, 7)  # Sum 3 numbers
print(hi_text())  # Print hello msg on terminal
print(subtract_2_numbers(8, 9))  # Print sum of subtract on terminal
obj = create_obj()  # Create new obj from classes
# Check created obj is instance of class ObjCreatedInFnc
print("Created obj is instance of \"ObjCreatedInFnc\" class:", isinstance(obj, ObjCreatedInFnc))

# Homework from page 50
homework_msg(50)  # Print information on terminal

# Creating objects from classes
MyPC()
MyComponents()
MyPcOverloading()

# Print on terminal
print("CPU:", MyComponents.cpu)
print("GPU:", MyComponents.gpu)

# Homework from page 56
print("\nHomework from page 56")  # Print information on terminal
# Print data about user name, last name and age on terminal
print(input("Please write you name: "), input("Please write you last name: "), input("Please write you age: "))
get_number()  # Get entered number from keyboard
flower(input("Please enter a flower name: "), input("Please enter a flower color: "))  # Ask about flower information

# Homework from page 71
homework_msg(71)  # Print information on terminal
user_welcome_mgs()  # Choosing greeting

# Check figure
rect_1 = Rectangle(4, 4)
rect_2 = Rectangle(8, 2)
title("Rectangle")  # Print header
print("Rectangle 1 is the same as rectangle 2 ->", rect_1 == rect_2)
print("Rectangle 1 isn't the same as rectangle 2 ->", rect_1 != rect_2)
print("Rectangle 1 is greater than rectangle 2 ->", rect_1 > rect_2)
print("Rectangle 1 is smaller than rectangle 2 ->", rect_1 < rect_2)
print("Rectangle 1 is greater or has this same size as rectangle 2 ->", rect_1 >= rect_2)
print("Rectangle 1 is smaller or has this same size as rectangle 2 ->", rect_1 <= rect_2)

# Check solid
cube_1 = Cube(2)
cube_2 = Cube(4)
title("Cube")  # Print header
print("Cube 1 has the same volume as cube 2 ->", cube_1 == cube_2)
print("Cube 1 has different volume than cube 2 ->", cube_1 != cube_2)
print("Cube 1 has greater volume than cube 2 ->", cube_1 > cube_2)
print("Cube 1 has smaller volume than cube 2 ->", cube_1 < cube_2)
print("Cube 1 has greater or this same volume as cube 2 ->", cube_1 >= cube_2)
print("Cube 1 has smaller or this same volume as cube 2 ->", cube_1 <= cube_2)

# Creating new object from class
mercedes = Mercedes(500000)
audi = Audi(450000)
bentley = Bentley(2100000)
maybach = Maybach(1750000)
lamborghini = Lamborghini(1500000)
ferrari = Ferrari(1200000)
lada = Lada(1000)
fso = Fso(2000)
opel = Opel(50000)
kia = Kia(45000)

# Check prise in car showroom
title("Car Showroom")  # Print header
print("Opel is cheaper than KIA ->", opel < kia)
print("Lamborghini is more expensive than Ferrari ->",  lamborghini > ferrari)
print("FSO has same price as Lada ->", fso == lada)
print("Bentley is cheaper or has this same price as Maybach ->", bentley <= maybach)
print("Mercedes is more expensive or has this same price as Audi->", mercedes >= audi)
print("Lada has different price as Lamborghini->", lada != lamborghini)

# Homework from page 82
homework_msg(82)  # Print information on terminal

# Creating lists
integers = [[1], [2], [3]]  # Creating list with 3 list inside
numbers_in_for = list()  # Creating empty list
floating_numbers = [.1, .2, .3, .4, .5]  # Creating list with 5 float elements

# Operation on list
for x in range(100, 116, 1):
    numbers_in_for.append(x)  # Add "x" element to array

# Print lists on terminal
print("integers:", integers)  # Print "integers" list on terminal
print("numbers_in_for:", numbers_in_for)  # Print "numbers_in_for" list on terminal
print("Content of \"floating_numbers\" list:")  # Print "floating_numbers" list on terminal

for x in floating_numbers:  # Print all elements in floating_numbers
    print(x)  # Print "x" element on terminal

# Operation not included in homework
print("\n\nThis amazing application is just finishing its operation. See you again (maybe?)")  # Print goodbye msg
