#!/usr/bin/enc python3
""" File with functions from homework """

# Import from other files here
from classes import ObjCreatedInFnc  # Get fnc_obj classes from classes.py file

__author__ = 'Mikołaj Błażejewski'
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT"
__version__ = ""
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Functions below
# Page 41


def author_name() -> None:
    """ Print info about author name """
    print("Mikołaj Błażejewski")


def add_3_numbers(num_1: int, num_2: int, num_3: int) -> None:
    """
    Sum 3 numbers
    :param num_1: int number
    :param num_2: int number
    :param num_3: int number
    :return None:
    """
    print(num_1 + num_2 + num_3)  # Print sum on terminal


def hi_text() -> str:
    """
     Return HELLO WORLD msg
    :return str: "Hello world"
    """
    return "Hello world"  # Return hello msg


def subtract_2_numbers(num_1: int, num_2: int) -> int:
    """
    Subtract 2 numbers
    :param num_1: int number
    :param num_2: int number
    :return int: Subtraction of arguments
    """
    return num_1 - num_2  # Return subtract


def create_obj() -> ObjCreatedInFnc:
    """
    Creating new object, checking that created obj is type of decelerated class
    :return fnc_obj: Created object
    """
    obj = ObjCreatedInFnc()  # Create new obj from class
    return obj  # Return obj

# Page 56


def get_number() -> None:
    """ Read entered value, then convert it in to number """
    number = input("Please enter a number: ")  # Get number from keyboard
    msg = "Value \"" + number + "\" isn't a number!"  # Create default msg
    if (not number.isalpha()) and number:
        msg = int(number)

    print(msg)


def flower(name: str, color: str) -> None:
    """
    Print data about flower entered from keyboard by user
    :param name: str - flower name
    :param color: str - flower color
    :return None:
    """
    print("The user's flower is a", name, "and has a", color, "color")

# Page 71


def user_welcome_mgs() -> None:
    """ Check user name and then choose greeting """
    welcome_msg = str("Welcome back guest")  # Decelerate default msg

    if input("Please enter your name: ") == "Mikołaj":
        welcome_msg = str("Welcome back my lord")

    print(welcome_msg)

# Operation not included in homework


def homework_msg(page: int) -> None:
    """
    Print information about page of homework
    :param page: int number - number of page
    :return None:
    """
    print("\n\nHomework from page", page)  # Print information on terminal


def title(header: str) -> None:
    """
    Print information about page of homework
    :param header: str - title to print
    :return None:
    """
    print("\n" + header, "-----------------------------------")  # Print information on terminal
