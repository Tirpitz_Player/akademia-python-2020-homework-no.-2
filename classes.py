#!/usr/bin/enc python3
""" File with classes from homework """

__author__ = "Mikołaj Błażejewski"
__copyright__ = "Copyright (c) 2020, Mikołaj Błażejewski"
__credits__ = ["Mikołaj Błażejewski"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = "Mikołaj Błażejewski"
__email__ = ""
__status__ = ""

# Declaring classes below
# Page 32


class Computer:
    """ Computer class """
    pass


class Component(Computer):
    """ Component class """
    pass


class CPU(Component):
    """ CPU class """
    pass


class GPU(Component):
    """ GPU class """
    pass


class PSU(Component):
    """ PSU class """
    pass

# Page 41


class ObjCreatedInFnc:
    """ Declaring a class for a task with a function """
    pass

# Page 50


class MyPC:
    """ MyPC class """
    cpu = str("AMD Ryzen 3600")
    gpu = str("Gigabyte RTX 270 Super")

    def __init__(self):
        """ Ctor """
        print("I have beautiful computer with", self.gpu, "and", self.cpu)


class MyComponents(MyPC):
    """ Variable class """
    pass


class MyPcOverloading(MyPC):
    """ Method function """

    def __init__(self):
        """ Ctor with overloading function """
        print("I can't take my eyes off my computer")
        super(MyPcOverloading, self).__init__()

# Page 71


class Rectangle:
    """ Rectangle class """
    def __init__(self, a: float, b: float) -> None:
        """
        Ctor
        :param a: float number - side length "a"
        :param b: float number - side length "b"
        """
        self.a = a
        self.b = b
        self.figure_field = a * b

    def __eq__(self, other):
        """ Equal """
        return (self.a == other.a) & (self.b == other.b)

    def __lt__(self, other):
        """ Less than """
        return self.figure_field < other.figure_field

    def __gt__(self, other):
        """ Greater than """
        return self.figure_field > other.figure_field

    def __le__(self, other):
        """ Less or equal """
        return self.figure_field <= other.figure_field

    def __ge__(self, other):
        """ Greater or equal """
        return self.figure_field >= other.figure_field

    def __ne__(self, other):
        """ Is not """
        return (self.a != other.a) & (self.b != other.b)


class Cube:
    """ Cube class """

    def __init__(self, a: float) -> None:
        """
        Ctor
        :param a: float number - side length
        """
        self.figure_vol = a ** 3

    def __eq__(self, other):
        """ Equal """
        return self.figure_vol == other.figure_vol

    def __lt__(self, other):
        """ Less than """
        return self.figure_vol < other.figure_vol

    def __gt__(self, other):
        """ Greater than """
        return self.figure_vol > other.figure_vol

    def __le__(self, other):
        """ Less or equal """
        return self.figure_vol <= other.figure_vol

    def __ge__(self, other):
        """ Greater or equal """
        return self.figure_vol >= other.figure_vol

    def __ne__(self, other):
        """ Is not """
        return self.figure_vol != other.figure_vol


class Car:
    """ Car class """
    def __init__(self, price: int) -> None:
        """
        Ctor
        :param price: int number - price of vehicle
        :return None:
        """
        self.price = price

    def __eq__(self, other):
        """ Equal """
        return self.price == other.price

    def __lt__(self, other):
        """ Less than """
        return self.price < other.price

    def __gt__(self, other):
        """ Greater than """
        return self.price > other.price

    def __le__(self, other):
        """ Less or equal """
        return self.price <= other.price

    def __ge__(self, other):
        """ Greater or equal """
        return self.price >= other.price

    def __ne__(self, other):
        """ Is not """
        return self.price != other.price


class Luxurious(Car):  # Car category
    """ Luxurious car class """
    pass


class Mercedes(Luxurious):
    """ Mercedes class """
    pass


class Audi(Luxurious):
    """ Audi class """


class SuperLuxurious(Car):  # Car category
    """ Luxurious car class """
    pass


class Bentley(SuperLuxurious):
    """ Bentley class """
    pass


class Maybach(SuperLuxurious):
    """ Maybach class """


class SuperSport(Car):  # Car category
    """ SuperSport car class """
    pass


class Lamborghini(SuperSport):
    """ Lamborghini class """
    pass


class Ferrari(SuperSport):
    """ Ferrari class """


class Sport(Car):  # Car category
    """ Sport car class """
    pass


class Porsche(Sport):
    """ Porsche class """
    pass


class Maserati(Sport):
    """ Maserati class """


class Cheap(Car):  # Car category
    """ Cheap car class """
    pass


class Lada(Cheap):
    """ Lada class """
    pass


class Fso(Cheap):
    """ Fso class """


class Popular(Car):  # Car category
    """ Popular car class """
    pass


class Opel(Popular):
    """ Opel class """
    pass


class Kia(Popular):
    """ Kia class """
    pass
