# Homework for Akademia Python 2019/2020

This is project with homework for Akademia Python 2019/2020

## Table of contents
* Technologies
* Setup
* Author of project

## Technologies
Project is created with:
* Python 3

## Setup

Run _start.py_ file to start app\
Shell command below:
~~~
python start.py
~~~

## Author of project
@Tirpitz_Player aka Mikołaj Błażejewski
